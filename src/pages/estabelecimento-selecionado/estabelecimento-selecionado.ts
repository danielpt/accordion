import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LanchePage } from '../lanche/lanche';
import { MeuPedidoPage } from '../meu-pedido/meu-pedido';

@Component({
  selector: 'page-estabelecimento-selecionado',
  templateUrl: 'estabelecimento-selecionado.html'
})

  export class EstabelecimentoSelecionadoPage {
      constructor(public navCtrl: NavController) {
  }

  goToLanche(params){
    if (!params) params = {};
    this.navCtrl.push(LanchePage);
  }goToMeuPedido(params){
    if (!params) params = {};
    this.navCtrl.push(MeuPedidoPage);
  }  

  
}
